/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief <Add Brief Description Here >
 *
 * <Add Extended Description Here>
 *
 * @author <Add FirsName LastName>
 * @date <Add date >
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Add Your Declarations and Function Comments here */ 


	void print_array(int SIZE, unsigned char n[SIZE]){
  	 	int i;
  	 	for (i = 0; i < SIZE; i++ ) {
      			printf("Element[%d] = %u\n", i, n[i] );
   		} 
      		
   	}
   	
   	void sort_array(int SIZE, unsigned char n[SIZE]){
   		int i,j,k;
   		for (i=0; i<SIZE; i++){
   			for (j=i+1; j<SIZE; j++){
   				if (n[i] > n[j]){
   					k = n[i];
   					n[i] = n[j];
   					n[j] = k;
   				}
   					
   			}printf("Element[%d] = %u\n", i, n[i] );
   			
   			
   			
   		}
   	}
   	
   	unsigned char find_maximum(int SIZE, unsigned char n[SIZE]){
  	 	int i;
  	 		for(i=0; i<SIZE; i++){
  	 		 	if (n[0] < n[i]){
  	 		 	n[0] = n[i];
      				}
      			}return n[0];
   	}	
   	
   	
   	
   	unsigned char find_minimum(int SIZE, unsigned char n[SIZE]){
  	 	int i;
  	 		for(i=0; i<SIZE; i++){
  	 		 	if (n[0] > n[i]){
  	 		 	n[0] = n[i];
      				}
      			}return n[0];
   	}
   	void print_statistics(min, max, mean, median){
   			
   		printf("Maximum number in the array is: %u\n", max);
   		printf("Minimum number in the array is: %u\n", min);
   		printf("The Mean of the array is: %u\n", mean);
   		printf("The Median of the array is: %u\n", median);
   		
   		
   	}
   	
   	unsigned char find_mean(int SIZE, unsigned char n[SIZE]){
   		int i,j,add,mean;
   		for (i=0; i<SIZE; i++){
   			add += n[i];
   			
   				
   		} mean = add/SIZE;
   					
   			}
   	unsigned char find_median(int SIZE, unsigned char n[SIZE]){
   		int i,j,mid,median, k;
   		for (i=0; i<SIZE; i++){
   			for (j=i+1; j<SIZE; j++){
   				if (n[i] > n[j]){
   					k = n[i];
   					n[i] = n[j];
   					n[j] = k;
   				}
   					
   			}
   			
   			
   			
   		}
   		mid = SIZE/2;
   		median = n[mid];
   		return median;
   	}
   			
   			
   			
   		
   	
   	
/**
 * @brief <Add Brief Description of Function Here>
 *
 * <Add Extended Description Here>
 *
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 * @param <Add InputName> <add description here>
 *
 * @return <Add Return Informaiton here>
 */


#endif /* __STATS_H__ */
